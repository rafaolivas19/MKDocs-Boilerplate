# MKDocs-Boilerplate

This boilerplate works as a starting point for new mkdocs projects

## Install dependencies

1. Install [Python](https://www.python.org/) (the package manager pip comes with it).
1. Install MKDocs with `pip install mkdocs`.
1. Install Material theme with `pip install mkdocs-material`

## Steps to start your project

1. **Clone:** Clone this repo.
1. **Delete .git:** Delete the ```.git``` directory inside the local repo.
1. **Edit mkdocs.yml:** In the file ```mkdocs.yml``` edit the next properties with the values of your choice:
	1. site_name.
	1. repo_name (you can leave it blank).
	1. repo_url (you can leave it blank).
1. **Set your custom image**: Place your custom image in the path ```assets/images/favicon.png``` (this value can be modified in the file ```mkdocs.yml```).